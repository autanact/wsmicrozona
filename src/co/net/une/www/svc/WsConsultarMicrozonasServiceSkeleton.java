
/**
 * WsConsultarMicrozonasServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsConsultarMicrozonasServiceSkeleton java skeleton for the axisService
     */
    public class WsConsultarMicrozonasServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsConsultarMicrozonasServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param microzonas
         */
        

                 public co.net.une.www.gis.WsConsultarMicrozonasRSType consultaMicrozona
                  (
                  co.net.une.www.gis.ListaMicrozonas microzonas
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("microzonas",microzonas);
		try{
		
			return (co.net.une.www.gis.WsConsultarMicrozonasRSType)
			this.makeStructuredRequest(serviceName, "consultaMicrozona", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    