
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package co.net.une.www.gis;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "MicrozonaRSType".equals(typeName)){
                   
                            return  co.net.une.www.gis.MicrozonaRSType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString100".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString100.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "ListaMicrozonas".equals(typeName)){
                   
                            return  co.net.une.www.gis.ListaMicrozonas.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "WsConsultarMicrozonas-RQ-Type".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsConsultarMicrozonasRQType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "WsConsultarMicrozonas-RS-Type".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsConsultarMicrozonasRSType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString15".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString15.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "ListaMicrozonasRS".equals(typeName)){
                   
                            return  co.net.une.www.gis.ListaMicrozonasRS.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString1024".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString1024.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString8".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString8.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "MicrozonaType".equals(typeName)){
                   
                            return  co.net.une.www.gis.MicrozonaType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString250".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString250.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString2".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString2.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString512".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString512.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "RespuestaProcesoType".equals(typeName)){
                   
                            return  co.net.une.www.gis.RespuestaProcesoType.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    